##Menu utilizando un while

def function1():
    print("Esta es la opcion 1")

def function2():
    print("Esta es la opcion 2")

def function3():
    print("Esta es la opcion 3")

def main():
        opcion = 0
        while(opcion != 5):
            print("********")
            print("1.- Funcion 1")
            print("2.- Funcion 2")
            print("3.- Funcion 3")
            print("4.- Gracias, byeee - Salir")
            print("********")
            opcion = input("Hola. Que opcion deseas? ")
            if( opcion == 1 ): function1()
            if( opcion == 2 ): function2()
            if( opcion == 3 ): function3()
            if( opcion == 4 ): break
        
        print("Adieu!!!")

main()