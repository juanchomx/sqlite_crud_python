import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn

def readSqliteTable(conn):
    try:
        sqliteConnection = conn
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sqlite_select_query = """SELECT * from projects"""
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        print("Total rows are:  ", len(records))
        print("Printing each row")
        for row in records:
            print("Id: ", row[0])
            print("Name: ", row[1]) 
            print("Begin Date: ", row[2])
            print("End Date: ", row[3])
            print("\n")

        cursor.close()

    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The SQLite connection is closed")    


def main():
    database = r"C:\sqlite\db\pythonsqlite.db"  

    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:
        ##print("Read tables")
        # read projects table
        readSqliteTable(conn)

    else:
        print("Error! cannot create the database connection.")              

if __name__ == '__main__':
    main()        